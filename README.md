## Spin up DHIS2 with docker
Change version of DHIS2 in ./dhis/Dockerfile

## Start
docker-compose build && docker-compose up -d

## Logging
docker logs -f dhis-postgres
docker logs -f dhis-dhis

## Fiddling on the boxes
docker exec -it dhis-postgres bash
docker exec -it dhis-dhis bash

## Access frontend
localhost:12345